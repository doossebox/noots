# Noots

This is just a note not a suggestion.

## Tricks

- [Alias](./tricks/alias.md)

**Programs:**

1. [yt-dlp](./tricks/ytdlp.md)
2. [dunst](./tricks/dunst.md)
3. [zathura](./tricks/zathura.md)
4. [mpd](./tricks/mpd.md)
5. [ncmpcpp](./tricks/ncmpcpp.md)
