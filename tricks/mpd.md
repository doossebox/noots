# mpd

Reference:

1. [Website](https://www.musicpd.org/)
2. [Github](https://github.com/MusicPlayerDaemon/MPD)

## Installation

**Archlinux:**

```bash
sudo pacman -S mpd
```

## Configuration

Location: `~/.config/mpd/mpd.conf`

```bash
music_directory       "~/documents/music"
playlist_directory    "~/.config/mpd/playlists"

auto_update "yes"
bind_to_address "localhost"
restore_paused "yes"
max_output_buffer_size "16384"

audio_output {
  type  "pulse"
  name  "pulse"
}

audio_output {
  type  "fifo"
  name  "Visualizer feed"
  path  "/tmp/mpd.fifo"
  format  "44100:16:2"
}
```
