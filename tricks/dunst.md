# Dunst

Reference:

[Github](https://github.com/dunst-project/dunst)

## Installing

**Archlinux:**

```bash
sudo pacman -S dunst
```

## Configuration

Location: `~/.config/dunst/dunstrc`

```bash
[global]
    monitor = 0
    follow = keyboard
    width = 370
    height = 350
    offset = 0x19
    padding = 2
    horizontal_padding = 2
    transparency = 10
    font = Jetbrains-mono 12
    format = "<b>%s</b>\n%b"
    dmenu = /usr/bin/dmenu -p dunst:

    mouse_left_click = close_current
    mouse_middle_click = do_action, close_current
    mouse_right_click = close_all

[urgency_low]
    background = "#212121"
    foreground = "#FFDF91"
    frame_color = "#7CB855"
    timeout = 3

[urgency_normal]
    foreground = "#FFDF91"
    background = "#212121"
    frame_color = "#00ADB5"
    timeout = 5

[urgency_critical]
    background = "#212121"
    foreground = "#FFDF91"
    frame_color = "#E72929"
    timeout = 10

# vim: ft=cfg
```
