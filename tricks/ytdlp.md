# yt-dlp

Reference:

[Github](https://github.com/yt-dlp/yt-dlp)

## Install yt-dlp

**Archlinux:**

```bash
sudo pacman -S yt-dlp ffmpeg atomicparsley python-mutagen python-pycryptodome \
python-websockets python-brotli python-xattr python-secretstorage
```

## Configuration

### Audio

Location: `~/.config/yt-dlp/audioconfig`

```bash
--extract-audio
--audio-quality 0
--audio-format mp3
--no-mtime
-o '$HOME/downloads/music/%(title)s.%(ext)s'
```

Location: `~/.bash_profile` or `~/.profile` or `~/.bashrc`

```bash
alias ytaudio='yt-dlp --config-location ~/.config/yt-dlp/audioconfig'
```

How to use:

```bash
ytaudio *link youtube*
```

### Video

#### Youtube

Location: `~/.config/yt-dlp/ytconfig`

```bash
--embed-thumbnail
--merge-output-format mp4
-f 'bv*+ba'
-o '$HOME/downloads/yt_%(id)s.%(ext)s'
```

Location: `~/.bash_profile` or `~/.profile` or `~/.bashrc`

```bash
alias ytvideo='yt-dlp --config-location ~/.config/yt-dlp/ytconfig'
```

How to use:

```bash
ytvideo *link youtube*
```

#### Vidio

Location: `~/.config/yt-dlp/vidconfig`

```bash
--output "$HOME/downloads/video/vidio_%(title)s.%(ext)s"
--check-formats
--sub-format best
--sub-langs id
--embed-thumbnail
--embed-subs
```

Location: `~/.bash_profile` or `~/.profile` or `~/.bashrc`

```bash
alias downvidio='yt-dlp --config-location ~/.config/yt-dlp/vidconfig'
```

How to use:

```bash
downvidio *link vidio*
```
