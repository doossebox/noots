# Alias

location: `.bash_aliases` or `.bashrc`

**yt-dlp:**

```bash
alias downvidio='yt-dlp --config-location ~/.config/yt-dlp/vidioconfig'
alias ytaudio='yt-dlp --config-location ~/.config/yt-dlp/audioconfig'
alias ytvideo='yt-dlp --config-location ~/.config/yt-dlp/videoconfig'
```

**ls:**

```bash
alias l='ls -Ap --sort=extension --group-directories-first'
alias la='ls -lAhp --sort=extension --group-directories-first'
alias ll='ls -lh'
alias ls='ls --color=auto'
```

**xclip:**

```bash
alias clip='xclip -sel clipboard'
```

**nmcli:**

```bash
alias wificonnect='nmcli --ask device wifi connect'
alias wifilist='nmcli device wifi list'
alias wifipass='nmcli dev wifi show-password'
```

**Random:**

```bash
alias grep='grep --color=auto'
alias openports='ss --all --numeric --processes --ipv4 --ipv6'
```
